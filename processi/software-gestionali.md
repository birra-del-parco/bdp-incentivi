# Software gestionali

- [Gestionali birrifici italiani](#gestionali-birrifici-italiani)
- [Gestionali birrifici esteri](#gestionali-birrifici-esteri)
- [Costi sviluppo gestionale ad hoc](#costi-sviluppo-gestionale-ad-hoc)
	- [Esempi](#esempi)
	- [Valutazioni personali](#valutazioni-personali)

## Gestionali birrifici italiani
- [Brew Calc](https://www.brewcalc.it) (Solo produzione, no fiscalità)
- [TeamSystem - Wine & Oil](https://www.teamsystem.com/wine-oil)
- [Webinar TeamSystem per i birrifici](https://www.teamsystem.com/webinar-teamsystem-wineoil-20200922)
- [Brewmatic](http://www.brewmatic.it) (estensione J-Beer per l'automazione 4.0)
- [inDogana](https://www.hivenet.it/prodotti/programma-software-accise/microbirrificio/)

## Gestionali birrifici esteri
- [BREWW](https://breww.com)
- [Orchestra](https://www.orchestrasoftware.com/breweries/)
- [Brewd](https://www.getbrewd.com)

## Costi sviluppo gestionale ad hoc
### Esempi
- [Idee costo 1](https://www.gardainformatica.it/blog/sviluppo-software/quanto-costa-sviluppare-una-soluzione-informatica-su-misura) ≈ 30-40k
- [Idee costo 2](https://www.opengate.biz/quanto-costa-un-software/) ≈ 20k
- Esperienza personale = 25k

### Valutazioni personali
- Solo processi **15-20k**
- Fiscalità **+5-10k**
- Integrazione con sistemi terzi complessi (contalitri, dogane, ecc) **+10-20k**
- Strutturazione del software per la vendita a terzi **+10k-?**

TOT: **50-60k**

> La stima dei costi è fatto su un software web-based

**Documentazione di reference** -> [Manuale comunicazione con sito delle dogane](https://telematicoprova.adm.gov.it/manuali/manuale_utente.pdf)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*