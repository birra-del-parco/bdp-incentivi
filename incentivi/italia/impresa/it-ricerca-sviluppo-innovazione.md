# Credito di imposta R&S&I

- [Descrizione](#descrizione)
- [Articolazione](#articolazione)

## Descrizione
Il **[Credito di imposta R&S&I](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-r-s)** si pone l’obiettivo di stimolare la spesa privata in Ricerca, Sviluppo e Innovazione tecnologica per sostenere la competitività delle imprese e per favorirne i processi di transizione digitale e nell’ambito dell’economia circolare e della sostenibilità ambientale.

La misura è di tipo: **Credito d'imposta**

> !!! - Vecchio credito di imposta R&I\
> La misura si espande per integrare ricerca, sviluppo, innovazione e design

## Articolazione

Attività di **ricerca fondamentale, ricerca industriale e sviluppo sperimentale** in campo scientifico e tecnologico:

- il credito d’imposta è riconosciuto in misura pari al 12% delle spese agevolabili nel limite massimo di 3 milioni di euro.

Attività di **innovazione tecnologica** finalizzate alla realizzazione di prodotti o processi di produzione nuovi o sostanzialmente migliorati:

- il credito d’imposta è riconosciuto in misura pari al 6% delle spese agevolabili nel limite massimo di 1,5 milioni di euro
- il credito d’imposta è riconosciuto in misura pari al 10% delle spese agevolabili nel limite massimo di 1,5 milioni di euro in caso di attività di innovazione tecnologica finalizzate al raggiungimento di un obiettivo di **transizione ecologica** o di **innovazione digitale 4.0**.

Attività di **design e ideazione estetica** per la concezione e realizzazione dei nuovi prodotti e campionari nei settori tessile e della moda, calzaturiero, dell’occhialeria, orafo, del mobile e dell’arredo e della ceramica, e altri individuati con successivo decreto ministeriale:

- il credito d’imposta è riconosciuto in misura pari al 6% delle spese agevolabili nel limite massimo di 1,5 milioni di euro.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*