# Patent Box

- [Descrizione](#descrizione)

## Descrizione
Il **[Patent Box](https://www.mise.gov.it/index.php/it/incentivi/impresa/patent-box)** è un regime opzionale di tassazione per i redditi d'impresa derivanti dall’utilizzo di software protetto da copyright, di brevetti industriali, di disegni e modelli, nonché di processi, formule e informazioni relativi ad esperienze acquisite nel campo industriale, commerciale o scientifico giuridicamente tutelabili.

La misura è di tipo: **Regime opzionale di tassazione**

**Spiegazione semplificata** -> [Patent Box 2019](https://www.startupbusiness.it/patent-box-2019-cose-normativa-e-come-accedere-alle-agevolazioni/100670/)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*