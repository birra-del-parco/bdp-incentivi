# Bonus pubblicità 2020

- [Descrizione](#descrizione)
- [Leggi di riferimento](#leggi-di-riferimento)
- [Fonti](#fonti)

## Descrizione
Il **[bonus pubblicità 2020](https://www.gazzettaufficiale.it/eli/id/2018/07/24/18G00115/SG)** prevedeva un'erogazione tramite credito di imposta da utilizzarsi in compensazione F24 fino al 50% della spesa per pubblicità su Televisioni, Giornali (sia cartacei che digitali) e Radio.

**SCADUTO: La domanda era da presentare entro il 30 settembre 2020**

## Leggi di riferimento
Articolo 5, comma 1, del D.P.C.M. n. 90 del 2018

## Fonti
- [Bonus pubblicità 2020](https://principemorici.it/2020/04/27/bonus-pubblicita-2020-ecco-le-agevolazioni-fiscali-e-le-novita-introdotte-dal-decreto-cura-italia/)
- [Bonus pubblicità 2020 2](https://www.gazzettaufficiale.it/eli/id/2018/07/24/18G00115/SG)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*