# Marchio e IP Assets

- [Descrizione](#descrizione)

## Descrizione
Attualmente Birra del Parco ha registrato solo il marchio testuale e nessun marchio visivo o di altro genere. Il marchio è registrato solo a livello nazionale.

Nessun disegno modello, brevetto o modello di utilità registrato.

> **???** - Verificare registrazione di una ricetta agroalimentare

Marchi+3 -> 4m Scorte esaurite il 10 giugno 2020

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*