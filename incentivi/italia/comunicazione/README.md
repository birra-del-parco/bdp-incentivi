# Incentivi - Italia / Comunicazione

## [Gadget aziendali](it-gadget-aziendali.md)
Le spese per i gadget sono **deducibili** dal reddito di impresa e **detraibili** lato IVA nella misura del 100%.

## [Spese di rappresentanza](it-spese-di-rappresentanza.md)
Le spese di rappresentanza hanno il vincolo della gratuità imprescindibile. Non potrebbero essere sfruttate quindi per eventi dove si vende al pubblico. Potrebbero essere invece interessanti per gli eventi promozionali lato B2B.

## [Marchio e IP Assets](it-marchio-ip.md)
*Yet to be filed*

## [Bonus Pubblicità 2020](it-bonus-pubblicità-2020.md) - SCADUTO
Il **[bonus pubblicità 2020](https://www.gazzettaufficiale.it/eli/id/2018/07/24/18G00115/SG)** prevedeva un'erogazione tramite credito di imposta da utilizzarsi in compensazione F24 fino al 50% della spesa per pubblicità su Televisioni, Giornali (sia cartacei che digitali) e Radio.